/**
 * Ejemplo de refactorización
 * Author: Eduardo Juarez
 * Date: 29/sep/20
 **/

//Include libraries
#include <stdlib.h> //Standard library
#include <stdio.h> //Standard input / output
#include <assert.h> //Testing library
#include <time.h>

void set_grades(float student_grades[100][4]) {

    int i;
    for (i = 0; i < 100; i++) {

        //Set grades of first 2 partials
        int j;
        for (j = 0; j < 3; j++) {
            student_grades[i][j] = 1.0 + rand() % 100;
        }

        //Set final grade
        student_grades[i][3] = ((student_grades[i][0] + student_grades[i][1]) / 2.0) * 0.6
                               + student_grades[i][2] * 0.4;
    }
}

int get_approved_students(float student_grades[100][4]) {
    int result = 0;
    int i;
    for (i = 0; i < 100; i++) {
        if (student_grades[i][3] >= 70.0) {
            result++;
        }
    }
    return result;
}

float get_average(float student_grades[100][4]) {
    float grades_sum = 0.0;
    int i;
    for (i = 0; i < 100; i++) {
        grades_sum += student_grades[i][3];
    }
    const float result = grades_sum / 100.0;
    return result;
}

/**
 * Program to grade students
 */
int main() {
    srand(time(NULL));
    float student_grades[100][4];

    set_grades(student_grades);

    //Print results
    printf("Alumnos aprobados: %i\nPromedio del grupo: %.2f",
           get_approved_students(student_grades),
           get_average(student_grades));

    return 0;
}
